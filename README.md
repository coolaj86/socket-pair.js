# SocketPair

[![NPM version](https://badge.fury.io/js/socket-pair.svg)](http://badge.fury.io/js/socket-pair)

A pair of coupled Unix sockets (or Windows pipes).

Similar to `stream-pair`, but with sockets with real fds and `.setTimeout()`.
Originally a workaround for <https://github.com/nodejs/node/issues/12716>,
but still has some use relevant cases, particularly when building proxies.

**Note**: This workaround is necessary in node v6.11.1, but not necessary in node v8.2.1.
I would assume it is also not necessary in later versions.

## Usage

```javascript
var socketPair = require('socket-pair');

var socket = socketPair.create(function (err, other) {
  // socket as in `client = new net.Socket(); client.connect(...);`
  // other as in `server.on('connection', function (conn) { ... })`

  socket.write('123');
  other.on('data', function (chunk) {
    console.log(chunk.toString('utf8'));
  });

  socketPair.closeAll();
});
```

I named them `client` and `connection`, but their names really have no meaning.

You can call them `a` and `b` or `other` and `one` or `red` and `blue`. It makes no difference.

## API

```
socketPair.create(cb)     // creates or reuses a socket server
socketPair.closeAll()     // closes the server and all sockets
```
