'use strict';

var PromiseA = require('bluebird');
var promises = {};

function createServer(prefix) {
	var os = require('os');
	var net = require('net');
	var path = require('path');
  var sockname = prefix + '.' + require('crypto').randomBytes(16).toString('hex') + '.sock';

  var sock;
  if (/^win/.test(os.platform())) {
    sock = path.join('\\\\?\\pipe', process.cwd(), sockname);
  }
  else {
    sock = path.join(os.tmpdir(), sockname);
  }

  var server = net.createServer({allowHalfOpen: true});
  server.unref();
  promises[prefix] = new PromiseA(function (resolve, reject) {
    server.once('error', reject);

    server.listen(sock, function () {
      server.removeListener('error', reject);
      resolve({sock: sock, server: server});
    });
  });

  server.on('close', function () {
    delete promises[prefix];
  });

  return promises[prefix];
}

exports.create = function create(cb, prefix) {
  prefix = prefix || 'node-socket-pair';
	var net = require('net');
	var client = new net.Socket({allowHalfOpen: true});

  if (!promises[prefix]) {
    createServer(prefix);
  }

  // We chain the promises to make sure that we never have multiple pending connections at
  // the same time to make sure the pairs are always matched correctly. Otherwise two different
  // `onConn` listeners might end up with the same connection.
  promises[prefix] = promises[prefix].then(function (result) {
    return new PromiseA(function (resolve) {
      function onConn(conn) {
        cb(null, conn);
        resolve(result);
      }
      result.server.once('connection', onConn);

      function onErr(err) {
        result.server.removeListener('connection', onConn);
        cb(err);
        resolve(result);
      }
      client.once('error', onErr);
      client.connect(result.sock, function () {
        client.removeListener('error', onErr);
      });
    });
  }, function (err) {
    cb(err);
    return PromiseA.reject(err);
  });

  return client;
};

exports.closeAll = function () {
  Object.keys(promises).forEach(function (key) {
    promises[key].then(function (result) {
      result.server.close();
    }, function () {
      delete promises[key];
    });
  });
};
